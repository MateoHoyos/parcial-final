import math as m


#PUNTO NUMERO 1

g=9.8

def calcularMovimiento (angulo,U,M1):
    lista= []
    M2= 0
    a= -1
    while a<0:
        a=m.ceil(g*((M2-(M1*m.sin(m.radians(angulo)))-(M1*U*m.cos(m.radians(angulo))))/(M1+M2)))
        if a<0:
            lista.append([M2,"Subiendo"])
            M2+=0.5
        else:
            lista.append([M2,"Bajando"])
    return lista

X=calcularMovimiento (55,0.15,6)
print (X)

#PUNTO NUMERO 2

def angulo_en_equilibrio (M1,M2,U):
    M = M2 / M1
    try:
        angsin1 = (M+(U*m.sqrt(1-m.pow(M,2)-m.pow(U,2))))/(1+m.pow(U,2))
        angsin2 = (M-(U*m.sqrt(1-m.pow(M,2)-m.pow(U,2))))/(1+m.pow(U,2))
        Ang1 = m.degrees(m.asin(angsin1))
        Ang2 = m.degrees(m.asin(angsin2))
    except ValueError:
        return -1
    
    if 85 > Ang1>10 and 85> Ang2 > 10: return Ang1, Ang2
    elif 85 > Ang1 >10 : return Ang1
    elif 85 > Ang2 >10 : return Ang2
    else: return -1

y=angulo_en_equilibrio (8,6,0.15)

print(y)